package com.hcloud.auth.clientdetails.entity;

import com.hcloud.common.crud.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "oauth_client_details",
        indexes = {@Index(name = "idx_client_id", columnList = "clientId")},
                uniqueConstraints = {@UniqueConstraint(columnNames="clientId")}
)
public class ClientEntity extends BaseEntity {
    private String clientId;
    private String resourceIds;
    private String clientSecret;
    private String scope;
    private String authorizedGrantTypes;
    private String webServerRedirectUri;
    private String authorities;
    private Integer accessTokenValidity;
    private Integer refreshTokenValidity;
    private String additionalInformation;
    private String autoapprove;
}
