package com.hcloud.auth.clientdetails.controller;

import com.hcloud.auth.clientdetails.entity.ClientEntity;
import com.hcloud.auth.clientdetails.service.ClientService;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.annontion.OperateLog;
import com.hcloud.common.core.annontion.OperatePostfix;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.core.constants.OperateType;
import com.hcloud.common.crud.annon.HCloudPreAuthorize;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/client")
@AllArgsConstructor
@AuthPrefix(AuthConstants.AUTH_CLIENT_PREFIX)
@OperatePostfix("clientDetail")
public class ClientController extends BaseDataController<ClientEntity> {

    private final ClientService clientService;

    @Override
    public BaseDataService getBaseDataService() {
        return clientService;
    }

    @PostMapping("/modifyPass")
    @OperateLog(title = "修改Client密码", type = OperateType.UPDATE)
    public HCloudResult modifyPass(String id, String oldPsw, String newPsw) {
        this.clientService.modifyPass(id, oldPsw, newPsw);
        return new HCloudResult();
    }

    @PostMapping("/resetPass")
    @OperateLog(title = "重置Client密码", type = OperateType.UPDATE)
    @HCloudPreAuthorize(AuthConstants.EDIT)
    public HCloudResult resetPass(String id) {
        this.clientService.resetPass(id);
        return new HCloudResult();
    }
}
