package com.hcloud.common.core.constants;

/**
 * 服务名称常量
 * @Auther hepangui
 * @Date 2018/11/8
 */
public interface ServerNameContants {
    String AUTH ="hcloud-auth";
    String SYSTEM = "hcloud-system";
    String AUDIT = "hcloud-audit";
}
