package com.hcloud.system.role.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.hcloud.common.crud.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "h_system_role_auth",
        indexes = {@Index(name = "idx_roleId", columnList = "roleId")
                , @Index(name = "idx_authId", columnList = "authId")}
)
public class RoleAuthEntity extends BaseEntity {
    private String roleId;
    private String authId;

}
