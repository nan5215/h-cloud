package com.hcloud.system.user.service.impl;

import com.hcloud.common.core.constants.CoreContants;
import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import com.hcloud.common.core.base.User;
import com.hcloud.system.user.entity.UserEntity;
import com.hcloud.system.user.repository.UserRepository;
import com.hcloud.system.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Service
public class UserServiceImpl extends
        BaseDataServiceImpl<UserEntity,UserRepository>
        implements UserService {

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public UserEntity findByAccount(String account) {
        UserEntity entity = this.baseRepository.findByAccount(account);
        if (entity == null) {
            throw new ServiceException("没有查到对用的用户:" + account);
        }
        return entity;
    }

    @Override
    public UserEntity findByMobile(String mobile) {
        UserEntity entity = this.baseRepository.findByMobile(mobile);
        if (entity == null) {
            throw new ServiceException("没有查到对用的用户:" + mobile);
        }
        return entity;
    }

    @Override
    public UserEntity findByWeixin(String weixin) {
        UserEntity entity = this.baseRepository.findByWechat(weixin);
        if (entity == null) {
            throw new ServiceException("没有查到对用的用户:" + weixin);
        }
        return entity;
    }

    @Override
    public UserEntity findByQq(String qq) {
        UserEntity entity = this.baseRepository.findByQq(qq);
        if (entity == null) {
            throw new ServiceException("没有查到对用的用户:" + qq);
        }
        return entity;
    }

    @Override
    public void modifyPass(User user, String oldPsw, String newPsw) {
        if (user == null || user.getId() == null) {
            throw new ServiceException("找不到用户");
        }
        UserEntity userEntity = this.get(user.getId());
        if (userEntity == null) {
            throw new ServiceException("找不到用户");
        }
        if (oldPsw != null && passwordEncoder.matches(oldPsw, userEntity.getPassword())) {
            userEntity.setPassword(passwordEncoder.encode(newPsw));
            this.update(userEntity);
        } else {
            throw new ServiceException("旧密码错误");
        }
    }

    @Override
    public void resetPass(String userId) {
        if (userId == null) {
            throw new ServiceException("找不到用户");
        }
        UserEntity userEntity = this.get(userId);
        if (userEntity == null) {
            throw new ServiceException("找不到用户");
        }
        userEntity.setPassword(passwordEncoder.encode(CoreContants.DEFAULT_PASSWORD));
        this.update(userEntity);
    }

    @Override
    public UserEntity add(UserEntity entity) {
        if (entity != null && entity.getAccount() != null) {
            entity.setPassword(new BCryptPasswordEncoder().encode(CoreContants.DEFAULT_PASSWORD));
        }
        return super.add(entity);
    }

    @Override
    public UserEntity update(UserEntity entity) {
        UserEntity entity1 = this.get(entity.getId());
        if (entity1 == null) {
            throw new ServiceException("用户错误");
        }
        String password = entity1.getPassword();
        BeanUtils.copyProperties(entity, entity1);
        entity1.setPassword(password);
        return super.update(entity1);
    }

}
