package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @Auther hepangui
 * @Date 2018/10/26
 */
@EnableConfigServer
@SpringCloudApplication
public class HCloudConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(HCloudConfigApplication.class, args);
    }

}
