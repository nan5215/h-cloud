package com.hcloud.audit.config;

import com.hcloud.auth.api.config.AbstractOAuth2ResourceServceConfig;
import com.hcloud.common.core.constants.ResourceConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * @author hepangui
 * @date 2018/10-31
 */
@EnableResourceServer
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuditResourceServceConfig extends AbstractOAuth2ResourceServceConfig {
    @Override
    protected String getResourceId() {
        return ResourceConstants.AUDIT;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/actuator/**"
                        , "/v2/api-docs"
                        , "/login/save"
                        , "/operate/save"
                        , "/authorities/**").permitAll()
                .anyRequest().authenticated()
                .and().csrf().disable();
    }


}
