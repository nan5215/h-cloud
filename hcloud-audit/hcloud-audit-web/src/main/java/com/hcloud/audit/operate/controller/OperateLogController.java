package com.hcloud.audit.operate.controller;

import com.hcloud.audit.operate.entity.OperateLogEntity;
import com.hcloud.audit.operate.service.OperateLogService;
import com.hcloud.common.core.annontion.AuthPrefix;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/operate")
@AllArgsConstructor
@AuthPrefix(AuthConstants.AUDIT_OPERATE_PREFIX)
public class OperateLogController extends BaseDataController<OperateLogEntity> {
    private final OperateLogService operateLogService;


    @Override
    public BaseDataService getBaseDataService() {
        return operateLogService;
    }

    /**
     * 重写方法，放开权限
     *
     * @param bean
     * @return
     */
    @PostMapping(value = "/save")
    public HCloudResult save(@RequestBody OperateLogEntity entity) {
        return super.saveOne(entity);
    }
}
